package com.submission.newsapp.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.submission.gameapp.BuildConfig
import com.submission.newsapp.data.source.remote.network.ApiResponse
import com.submission.newsapp.data.source.remote.network.RetrofitBuilder
import com.submission.newsapp.model.BaseArticleResponse
import com.submission.newsapp.model.BaseSource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewsViewModel: ViewModel() {

    private val listCategory = MutableLiveData<ApiResponse<BaseSource>>()
    private val sourceList = MutableLiveData<ApiResponse<BaseSource>>()
    private val listArticles = MutableLiveData<ApiResponse<BaseArticleResponse>>()

    private val listFromSearch = MutableLiveData<ApiResponse<BaseArticleResponse>>()
    private val network: RetrofitBuilder = RetrofitBuilder()

    private var page = 1
    fun fetchNewsSource(){
        listCategory.postValue(ApiResponse.onLoading(null))
        network.api().showNewsSource(BuildConfig.API_KEY).enqueue(object : Callback<BaseSource> {
            override fun onResponse(call: Call<BaseSource>, response: Response<BaseSource>) {
                if (response.isSuccessful){
                    val baseResponse = response.body()
                    baseResponse?.let { base ->
                        if (base.status == "error"){
                            listCategory.postValue(ApiResponse.onError(base.status, null))
                            Log.d("fetchErr", "status fetch ${base.status}")
                        } else {
                            listCategory.postValue(ApiResponse.onSucces(response.body()))
                        }
                    }
                }
            }

            override fun onFailure(call: Call<BaseSource>, t: Throwable) {
                listCategory.postValue(ApiResponse.onError(t.message, null))
            }

        })
    }

    fun fetchListSource(category: String, search: String = ""){
        sourceList.postValue(ApiResponse.onLoading(null))
        network.api().showSourceByCategory(BuildConfig.API_KEY, category = category, sources = search).enqueue(object : Callback<BaseSource>{
            override fun onResponse(call: Call<BaseSource>, response: Response<BaseSource>) {
                if (response.isSuccessful) sourceList.postValue(ApiResponse.onSucces(response.body()))
            }

            override fun onFailure(call: Call<BaseSource>, t: Throwable) {
                sourceList.postValue(ApiResponse.onError(t.message, null))
            }

        })
    }

    fun fetchListArticles(q: String, sources: String, search: String = ""){
        listFromSearch.postValue(ApiResponse.onLoading(null))
        network.api().showArticleWithPaging(q, sources, BuildConfig.API_KEY, page, 10, search).enqueue(object : Callback<BaseArticleResponse>{
            override fun onResponse(
                call: Call<BaseArticleResponse>,
                response: Response<BaseArticleResponse>
            ) {
                if (response.isSuccessful) listFromSearch.postValue(ApiResponse.onSucces(response.body()))
            }

            override fun onFailure(call: Call<BaseArticleResponse>, t: Throwable) {
                listFromSearch.postValue(ApiResponse.onError(t.message, null))
            }

        })
    }

    fun fetchArticlesWithPaging(q: String, source: String, search: String = ""){
        listArticles.postValue(ApiResponse.onLoading(null))
        network.api().showArticleWithPaging(q, source, BuildConfig.API_KEY, page, 15, search).enqueue(object : Callback<BaseArticleResponse>{
            override fun onResponse(
                call: Call<BaseArticleResponse>,
                response: Response<BaseArticleResponse>
            ) {
                if (response.isSuccessful){
                    val baseResponse  = response.body()
                    if (baseResponse?.status == "error"){
                        listArticles.postValue(ApiResponse.onError(baseResponse.status, null))
                    } else {
                        listArticles.postValue(ApiResponse.onSucces(response.body()))
                        page++
                    }
                }
            }

            override fun onFailure(call: Call<BaseArticleResponse>, t: Throwable) {
                listArticles.postValue(ApiResponse.onError(t.message, null))
            }

        })
    }

    fun getNewsSource() : LiveData<ApiResponse<BaseSource>> = listCategory

    fun getListSource() : LiveData<ApiResponse<BaseSource>> = sourceList

    fun getListArticle() : LiveData<ApiResponse<BaseArticleResponse>> = listArticles

    fun getFromSearch() : LiveData<ApiResponse<BaseArticleResponse>> = listFromSearch
}